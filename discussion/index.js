// // JSON

// // JSON as objects
// {
//     "city": "Quezon City",
//     "province": "Metro Manila",
//     "country": "Philippines";
// };

// // JSON arrays
// "cities": [
//     {"city": "Quezon City"}
// ]

// // Mini Activity - Create a JSON Array that will hold three breeds of dogs with properties: name, age, and breed.
// "dog": [
//     {
//         "name": "Princess",
//         "age": "6",
//         "breed": "German Shepherd"
//     },
//     {
//         "name": "Destroyer",
//         "age": "8",
//         "breed": "Pitbull"
//     },
//     {
//         "name": "Luna",
//         "age": "3 months",
//         "breed": "Luna"
//     }
// ]

// JSON Methods

// Convert data into Stringified JSON

let batchesArr = [{ batchName: "Batch X" }, { batchName: "Batch Y" }];

// the "stringify" method is used to convert JavaScript objects into a string
console.log("Result from stringify method:");
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
  name: "John",
  age: 31,
  address: {
    city: "Manila",
    country: "Philippines",
  },
});

console.log(data);

// Using stringify method with variables
// User details
// let firstName = prompt("What is your first name");
// let lastName = prompt("What is your last name");
// let age = prompt("What is your age?");
// let address = {
//   city: prompt("Which city do you live in?"),
//   country: prompt("Which country does your city address belong to?"),
// };

// let otherData = JSON.stringify({
//   firstName: firstName,
//   lastName: lastName,
//   age: age,
//   address: address,
// });

// console.log(otherData);

// Mini Activity - Create a JSON data that will accept user car details with variables brand, type, year.

// let carBrand = prompt("What is your car brand? ");
// let carType = prompt("What is your car type? ");
// let carYear = prompt("What year was your car manufactured? ");

// let car = JSON.stringify({
//   carBrand: carBrand,
//   carType: carType,
//   carYear: carYear,
// });

// console.log(car);

// Converting Stringified JSON into JavaScript Objects

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));
